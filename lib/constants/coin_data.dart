import 'package:flutter/material.dart';

final coinList = {
  'AAVEUSD':
  {
    'name': 'Aave',
    'altname': 'AAVE',
    'imageUrl': 'images/aave.png',
    'icon': '',
    'iconBg': Colors.purpleAccent,
  },
  'ADAUSD':
  {
    'name': 'Cardano',
    'altname': 'ADA',
    'imageUrl': 'images/ada.png',
    'icon': '',
    'iconBg': Colors.blue.shade700,
  },
  'ALGOUSD':
  {
    'name': 'Algorand',
    'altname': 'ALGO',
    'imageUrl': 'images/algo.png',
    'icon': '',
    'iconBg': Colors.tealAccent.shade700,
  },
  'ANTUSD':
  {
    'name': 'Aragon',
    'altname': 'ANT',
    'imageUrl': 'images/ant.png',
    'icon': '',
    'iconBg': Colors.lightBlueAccent,
  },
  'ATOMUSD':
  {
    'name': 'Cosmos',
    'altname': 'ATOM',
    'imageUrl': 'images/atom.png',
    'icon': '',
    'iconBg': Colors.black,
  },
  'BALUSD':
  {
    'name': 'Balancer',
    'altname': 'BAL',
    'imageUrl': 'images/bal.png',
    'icon': '',
    'iconBg': Colors.tealAccent.shade700,
  },
  'BATUSD':
  {
    'name': 'Basic Attention Token',
    'altname': 'BAT',
    'imageUrl': 'images/bat.png',
    'icon': '',
    'iconBg': Colors.orangeAccent.shade700,
  },
  'BCHUSD':
  {
    'name': 'Bitcoin Cash',
    'altname': 'BCH',
    'imageUrl': 'images/bch.png',
    'icon': '',
    'iconBg': Colors.greenAccent.shade400,
  },
  'COMPUSD':
  {
    'name': 'Compound',
    'altname': 'COMP',
    'imageUrl': 'images/comp.png',
    'icon': '',
    'iconBg': Colors.greenAccent,
  },
  'CRVUSD':
  {
    'name': 'Curve DAO Token',
    'altname': 'CRV',
    'imageUrl': 'images/crv.png',
    'icon': '',
    'iconBg': Colors.blueGrey,
  },
  'DAIUSD':
  {
    'name': 'Dai',
    'altname': 'DAI',
    'imageUrl': 'images/dai.png',
    'icon': '',
    'iconBg': Colors.yellowAccent.shade700,
  },
  'DASHUSD':
  {
    'name': 'Dash',
    'altname': 'DASH',
    'imageUrl': 'images/dash.png',
    'icon': '',
    'iconBg': Colors.blue,
  },
  'DOTUSD':
  {
    'name': 'Polkadot',
    'altname': 'DOT',
    'imageUrl': 'images/dot.png',
    'icon': '',
    'iconBg': Colors.pinkAccent.shade700,
  },
  'EOSUSD':
  {
    'name': 'EOS',
    'altname': 'EOS',
    'imageUrl': 'images/eos.png',
    'icon': '',
    'iconBg': Colors.black,
  },
  'EWTUSD':
  {
    'name': 'Energy Web Token',
    'altname': 'EWT',
    'imageUrl': 'images/ewt.png',
    'icon': '',
    'iconBg': Colors.purple.shade400,
  },
  'FILUSD':
  {
    'name': 'Filecoin',
    'altname': 'FIL',
    'imageUrl': 'images/fil.png',
    'icon': '',
    'iconBg': Colors.blue.shade600,
  },
  'FLOWUSD':
  {
    'name': 'Flow',
    'altname': 'FLOW',
    'imageUrl': 'images/flow.png',
    'icon': '',
    'iconBg': Colors.yellow.shade600,
  },
  'GNOUSD':
  {
    'name': 'Gnosis',
    'altname': 'GNO',
    'imageUrl': 'images/gno.png',
    'icon': '',
    'iconBg': Colors.cyan.shade700,
  },
  'GRTUSD':
  {
    'name': 'The Graph',
    'altname': 'GRT',
    'imageUrl': 'images/grt.png',
    'icon': '',
    'iconBg': Colors.blue.shade600,
  },
  'ICXUSD':
  {
    'name': 'ICON',
    'altname': 'ICX',
    'imageUrl': 'images/icx.png',
    'icon': '',
    'iconBg': Colors.cyan.shade800,
  },
  'KAVAUSD':
  {
    'name': 'Kava.io',
    'altname': 'KAVA',
    'imageUrl': 'images/kava.png',
    'icon': '',
    'iconBg': Colors.black,
  },
  'KEEPUSD':
  {
    'name': 'Keep Network',
    'altname': 'KEEP',
    'imageUrl': 'images/keep.png',
    'icon': '',
    'iconBg': Colors.greenAccent,
  },
  'KNCUSD':
  {
    'name': 'Kyber Network',
    'altname': 'KNC',
    'imageUrl': 'images/knc.png',
    'icon': '',
    'iconBg': Colors.tealAccent.shade700,
  },
  'KSMUSD':
  {
    'name': 'Kusama',
    'altname': 'KSM',
    'imageUrl': 'images/ksm.png',
    'icon': '',
    'iconBg': Colors.black,
  },
  'LINKUSD':
  {
    'name': 'Chainlink',
    'altname': 'LINK',
    'imageUrl': 'images/link.png',
    'icon': '',
    'iconBg': Colors.blue.shade900,
  },
  'LSKUSD':
  {
    'name': 'Lisk',
    'altname': 'LSK',
    'imageUrl': 'images/lsk.png',
    'icon': '',
    'iconBg': Colors.blue.shade700,
  },
  'MANAUSD':
  {
    'name': 'Decentraland',
    'altname': 'MANA',
    'imageUrl': 'images/mana.png',
    'icon': '',
    'iconBg': Colors.redAccent.shade400,
  },
  'NANOUSD':
  {
    'name': 'Nano',
    'altname': 'NANO',
    'imageUrl': 'images/nano.png',
    'icon': '',
    'iconBg': Colors.blue.shade300,
  },
  'OCEANUSD':
  {
    'name': 'Ocean Protocol',
    'altname': 'OCEAN',
    'imageUrl': 'images/ocean.png',
    'icon': '',
    'iconBg': Colors.pinkAccent,
  },
  'OMGUSD':
  {
    'name': 'OMG Network',
    'altname': 'OMG',
    'imageUrl': 'images/omg.png',
    'icon': '',
    'iconBg': Colors.blueAccent.shade700,
  },
  'OXTUSD':
  {
    'name': 'Orchid',
    'altname': 'OXT',
    'imageUrl': 'images/oxt.png',
    'icon': '',
    'iconBg': Colors.purpleAccent.shade700,
  },
  'PAXGUSD':
  {
    'name': 'PAX Gold',
    'altname': 'PAXG',
    'imageUrl': 'images/paxg.png',
    'icon': '',
    'iconBg': Colors.yellow.shade600,
  },
  'QTUMUSD':
  {
    'name': 'Qtum',
    'altname': 'QTUM',
    'imageUrl': 'images/qtum.png',
    'icon': '',
    'iconBg': Colors.cyanAccent.shade700,
  },
  'REPV2USD':
  {
    'name': 'Augur v2',
    'altname': 'REPV2',
    'imageUrl': 'images/rep2.png',
    'icon': '',
    'iconBg': Colors.deepPurple.shade600,
  },
  'SCUSD':
  {
    'name': 'Siacoin',
    'altname': 'SC',
    'imageUrl': 'images/sc.png',
    'icon': '',
    'iconBg': Colors.greenAccent.shade200,
  },
  'SNXUSD':
  {
    'name': 'Synthetix',
    'altname': 'SNX',
    'imageUrl': 'images/snx.png',
    'icon': '',
    'iconBg': Colors.deepPurple,
  },
  'STORJUSD':
  {
    'name': 'Storj',
    'altname': 'STORJ',
    'imageUrl': 'images/storj.png',
    'icon': '',
    'iconBg': Colors.blue.shade900,
  },
  'TBTCUSD':
  {
    'name': 'tBTC',
    'altname': 'TBTC',
    'imageUrl': 'images/tbtc.png',
    'icon': '',
    'iconBg': Colors.black,
  },
  'TRXUSD':
  {
    'name': 'TRON',
    'altname': 'TRX',
    'imageUrl': 'images/trx.png',
    'icon': '',
    'iconBg': Colors.red.shade800,
  },
  'UNIUSD':
  {
    'name': 'Uniswap',
    'altname': 'UNI',
    'imageUrl': 'images/uni.png',
    'icon': '',
    'iconBg': Colors.pink,
  },
  'USDCUSD':
  {
    'name': 'USD Coin',
    'altname': 'USDC',
    'imageUrl': 'images/usdc.png',
    'icon': '',
    'iconBg': Colors.blue.shade700,
  },
  // 'USDTZUSD':
  // {
  //   'name': 'Tether',
  //   'altname': 'USDT',
  //   'imageUrl': 'images/usdt.png',
  //   'icon': '',
  //   'iconBg': Colors.green,
  // },
  'WAVESUSD':
  {
    'name': 'Waves',
    'altname': 'WAVES',
    'imageUrl': 'images/waves.png',
    'icon': '',
    'iconBg': Colors.blueAccent.shade700,
  },
  'XETCZUSD':
  {
    'name': 'Ethereum Classic',
    'altname': 'ETC',
    'imageUrl': 'images/etc.png',
    'icon': '',
    'iconBg': Colors.greenAccent.shade400,
  },
  'XETHZUSD':
  {
    'name': 'Ethereum',
    'altname': 'ETH',
    'imageUrl': 'images/eth.png',
    'icon': '',
    'iconBg': Colors.black,
  },
  'XLTCZUSD':
  {
    'name': 'Litecoin',
    'altname': 'LTC',
    'imageUrl': 'images/ltc.png',
    'icon': '',
    'iconBg': Colors.indigo.shade700,
  },
  'XMLNZUSD':
  {
    'name': 'Enzyme',
    'altname': 'MLN',
    'imageUrl': 'images/mln.png',
    'icon': '',
    'iconBg': Colors.black,
  },
  'XREPZUSD':
  {
    'name': 'Augur',
    'altname': 'REP',
    'imageUrl': 'images/rep.png',
    'icon': '',
    'iconBg': Colors.deepPurple.shade900,
  },
  'XTZUSD':
  {
    'name': 'Tezos',
    'altname': 'XTZ',
    'imageUrl': 'images/xtz.png',
    'icon': '',
    'iconBg': Colors.blue.shade700,
  },
  'XXBTZUSD':
  {
    'name': 'Bitcoin',
    'altname': 'XBT',
    'imageUrl': 'images/xbt.png',
    'icon': '',
    'iconBg': Colors.yellow.shade700,
  },
  'XDGUSD':
  {
    'name': 'Dogecoin',
    'altname': 'XDG',
    'imageUrl': 'images/xdg.png',
    'icon': '',
    'iconBg': Colors.yellowAccent.shade700,
  },
  'XXLMZUSD':
  {
    'name': 'Stellar',
    'altname': 'XLM',
    'imageUrl': 'images/xlm.png',
    'icon': '',
    'iconBg': Colors.lightBlue.shade300,
  },
  'XXMRZUSD':
  {
    'name': 'Monero',
    'altname': 'XMR',
    'imageUrl': 'images/xmr.png',
    'icon': '',
    'iconBg': Colors.orange.shade600,
  },
  'XXRPZUSD':
  {
    'name': 'XRP',
    'altname': 'XRP',
    'imageUrl': 'images/xrp.png',
    'icon': '',
    'iconBg': Colors.lightBlue.shade900,
  },
  'XZECZUSD':
  {
    'name': 'Zcash',
    'altname': 'ZEC',
    'imageUrl': 'images/zec.png',
     'icon': '',
    'iconBg': Colors.orangeAccent,
  },
  'YFIUSD':
  {
    'name': 'yearn.finance',
    'altname': 'YFI',
    'imageUrl': 'images/yfi.png',
    'icon': '',
    'iconBg': Colors.blueAccent,
  },
};