import 'package:flutter/material.dart';

final kPrimaryColor = Color(0xFF6200EE);
final kPrimaryVarColor = Color(0xFF3700B3);
final kSecondaryColor = Colors.cyan.shade300;
final kSecondaryVarColor = Colors.cyan.shade600;
final kAccentColor = Color(0xFFFE5679);
final kFontColor = Color(0xDD000000);
final kBackgroundColor = Colors.white;

final kGraphLabelStyle = TextStyle(
  fontSize: 12.0,
  color: Colors.black54,
  fontWeight: FontWeight.bold,
);

final kLabelSmallStyle = TextStyle(
  fontSize: 12.0,
  fontWeight: FontWeight.w600,
  color: Colors.black45,
);

final kLabelStyle = TextStyle(
  fontSize: 14.0,
  fontWeight: FontWeight.w400,
);

final kLabelBoldStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.bold,
  color: kFontColor,
);

final kLabelAccentBoldStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.bold,
  color: kAccentColor,
);

final kLabelBigBoldStyle = TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
);

final kLabelPlaceholderStyle = TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
  color: Colors.black26,
);

final kLabelPlaceholderAccentStyle = TextStyle(
  fontSize: 20.0,
  fontWeight: FontWeight.bold,
  color: kAccentColor,
);

final kAppBarBigTitleStyle = TextStyle(
  fontSize: 32.0,
  fontWeight: FontWeight.bold,
  color: kFontColor,
);

final kAppBarAltTitleStyle = TextStyle(
  fontSize: 16.0,
  fontWeight: FontWeight.w600,
  color: kFontColor,
);

final kAppBarSubTitleStyle = TextStyle(
  fontSize: 14.0,
  fontWeight: FontWeight.w600,
  color: kFontColor, // Colors.white,
);

final kNewsLabelStyle = TextStyle(
  color: kFontColor,
  fontSize: 14.0,
  fontWeight: FontWeight.w600,
);

final kLabelTransparent = TextStyle(color: Colors.transparent);
