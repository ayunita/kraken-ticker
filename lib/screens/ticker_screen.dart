import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:kraken_ticker/services/ticker.dart';
import 'package:kraken_ticker/components/ticker_card.dart';
import 'package:kraken_ticker/components/ohlc_chart.dart';
import 'package:kraken_ticker/components/sticky_header.dart';
import 'package:kraken_ticker/constants/styles.dart';
import 'package:kraken_ticker/constants/coin_data.dart';
import 'package:kraken_ticker/screens/detail_screen.dart';

/// TickerScreen contains a list of assets listed on Kraken marketplace.
class TickerScreen extends StatefulWidget {
  @override
  _TickerScreenState createState() => _TickerScreenState();
}

class _TickerScreenState extends State<TickerScreen> {
  Map ticker = {};
  final height = 120.0;
  final top = 0.0;

  @override
  void initState() {
    super.initState();
    _loadTickerInfo();
  }

  void _loadTickerInfo() async {
    Map data = await Ticker().getTicker(coinList);
    if (data.isNotEmpty) {
      var sorted = new SplayTreeMap.from(
          data, (a, b) => data[b]['trades'].compareTo(data[a]['trades']));
      setState(() => ticker = sorted);
    }
  }

  Widget _buildLineChart(String pair) {
    return FutureBuilder<dynamic>(
      future: Ticker().getOHLC(pair),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: Text('Loading'));
        } else {
          if (snapshot.hasError)
            return Center(child: Text('Error'));
          else
            return OHLCChart(
              points: snapshot.data,
              highest:
                  snapshot.data.reduce((a, b) => a['avg'] > b['avg'] ? a : b),
            );
        }
      },
    );
  }

  void _refreshTicker(pair) async {
    bool refresh = await Navigator.push(
      context,
      MaterialPageRoute(builder: (_) => DetailScreen(pair)),
    ) ?? true; // default to true, whether user clicked 'back' or swipe
    if (refresh) _loadTickerInfo();
  }

  Widget _buildTickerCard(dynamic pair) {
    var coin = coinList[pair];
    var coinTicker = ticker[pair];
    return GestureDetector(
      onTap: () => _refreshTicker(pair),
      child: TickerCard(
        name: (ticker.isNotEmpty) ? coin['name'] : '',
        altname: (ticker.isNotEmpty) ? coin['altname'] : '',
        bid: (ticker.isNotEmpty) ? coinTicker['bid'] : 0,
        ask: (ticker.isNotEmpty) ? coinTicker['ask'] : 0,
        icon: (ticker.isNotEmpty) ? coin['icon'] : '',
        iconBg: coin['iconBg'],
        size: 40.0,
        paddingH: 20.0,
        lineChart: _buildLineChart(pair),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: RefreshIndicator(
          onRefresh: () async => _loadTickerInfo(),
          child: CustomScrollView(
            slivers: [
              StickyHeader(
                top: top,
                height: height,
                icon: null,
                title: 'Kraken Market',
                subTitle: 'The most traded stock in 24 hours',
                altTitle: 'Most traded stock in 24h',
                titleStyle: kAppBarBigTitleStyle,
                subTitleStyle: kLabelStyle,
                altTitleStyle: kAppBarAltTitleStyle,
              ),
              SliverList(
                delegate: SliverChildListDelegate(
                  ticker.entries
                      .map((pair) => _buildTickerCard(pair.key))
                      .toList(),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
