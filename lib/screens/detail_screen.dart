import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kraken_ticker/services/ticker.dart';
import 'package:kraken_ticker/components/ohlc_chart.dart';
import 'package:kraken_ticker/components/sticky_header.dart';
import 'package:kraken_ticker/constants/coin_data.dart';
import 'package:kraken_ticker/constants/styles.dart';
import 'package:kraken_ticker/services/storage.dart';

/// DetailScreen contains ticker data and OHLC graph.
class DetailScreen extends StatefulWidget {
  final pair;
  DetailScreen(this.pair);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  Storage storage = Storage();
  String pair = '';
  Map ticker = {};
  bool watchlistEnabled = true;

  final height = 130.0;
  final top = 0.0;
  final size = 32.0;

  @override
  void initState() {
    super.initState();
    setState(() => pair = widget.pair);
    _loadTickerInfo();
    _loadWatchlist();
  }

  void _loadTickerInfo() async {
    Map data = await Ticker().getTicker({pair: coinList[pair]});
    if (data.isNotEmpty) {
      setState(() => ticker = data);
    }
  }

  void _loadWatchlist() async {
    bool exist = await storage.checkWatchlistExist(pair);
    setState(() => watchlistEnabled = !exist);
  }

  _formatNumber(num) => NumberFormat.decimalPattern().format(num);

  _buildLineChart() {
    if (ticker.isEmpty) return;

    return FutureBuilder<dynamic>(
      future: Ticker().getOHLC(pair),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: Text('Loading...'));
        } else {
          if (snapshot.hasError)
            return Center(child: Text('Error'));
          else
            return OHLCChart(
              points: snapshot.data,
              touchEnabled: true,
              showAxis: true,
              highest:
                  snapshot.data.reduce((a, b) => a['avg'] > b['avg'] ? a : b),
            );
        }
      },
    );
  }

  _buildPriceTitle() {
    if (ticker.isEmpty) return Container();
    return Container(
      padding: EdgeInsets.all(8.0),
      child: Text("\$${_formatNumber(ticker[pair]['ask'])}",
          textAlign: TextAlign.right,
          style: TextStyle(
            color: kPrimaryColor,
            fontSize: 32.0,
            fontWeight: FontWeight.bold,
          )),
    );
  }

  _buildTableCell(String label, dynamic value) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            '$label',
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
          Text(
            '$value',
            textAlign: TextAlign.right,
            style: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }

  _buildPriceInfo() {
    if (ticker.isEmpty) return Container();

    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 32.0,
      ),
      child: Table(
        columnWidths: {
          0: FlexColumnWidth(1),
          1: FlexColumnWidth(1),
        },
        children: [
          TableRow(children: [
            _buildTableCell(
                'Prev Close', _formatNumber(ticker[pair]['last_close'])),
            _buildTableCell('Open', _formatNumber(ticker[pair]['open'])),
          ]),
          TableRow(children: [
            _buildTableCell('Bid', _formatNumber(ticker[pair]['bid'])),
            _buildTableCell('Ask', _formatNumber(ticker[pair]['ask'])),
          ]),
          TableRow(children: [
            _buildTableCell('24h Low', _formatNumber(ticker[pair]['low'])),
            _buildTableCell('24h High', _formatNumber(ticker[pair]['high'])),
          ]),
          TableRow(children: [
            _buildTableCell(
                '24h Volume', _formatNumber(ticker[pair]['volume'])),
            _buildTableCell(
                '24h Trades', _formatNumber(ticker[pair]['trades'])),
          ]),
        ],
      ),
    );
  }

  void _setWatchlist() async {
    bool success = await storage.storeWatchlist(pair);
    if (success) {
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/', (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            StickyHeader(
              top: top,
              height: height,
              icon: Container(
                padding: EdgeInsets.symmetric(horizontal: 16.0),
                child: Center(
                  child: Text(
                    coinList[pair]['icon'],
                    style: TextStyle(
                      fontFamily: 'Cryptofont',
                      color: coinList[pair]['iconBg'],
                      fontSize: 48,
                    ),
                  ),
                ),
              ),
              title: coinList[pair]['altname'],
              subTitle: coinList[pair]['name'],
              altTitle:
                  "${coinList[pair]['altname']} ${coinList[pair]['name']}",
              titleStyle: kAppBarBigTitleStyle,
              subTitleStyle: kLabelStyle,
              altTitleStyle: kAppBarAltTitleStyle,
              rightTitleComponent: _buildPriceTitle(),
              rightTopComponent: IconButton(
                icon: FaIcon(
                  watchlistEnabled
                      ? FontAwesomeIcons.eye
                      : FontAwesomeIcons.eyeSlash,
                  color: kAccentColor,
                ),
                splashColor: Colors.black12,
                tooltip: 'Kraken Market',
                iconSize: 24.0,
                onPressed: _setWatchlist,
                highlightColor: Colors.transparent,
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  SizedBox(
                    height: 300.0,
                    child: _buildLineChart(),
                  ),
                  _buildPriceInfo()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
