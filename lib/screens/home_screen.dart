import 'package:flutter/material.dart';
import 'package:kraken_ticker/constants/styles.dart';
import 'package:kraken_ticker/services/storage.dart';
import 'package:kraken_ticker/services/ticker.dart';
import 'package:kraken_ticker/components/ticker_card.dart';
import 'package:kraken_ticker/components/ohlc_chart.dart';
import 'package:kraken_ticker/components/pill_button.dart';
import 'package:kraken_ticker/components/news_filter.dart';
import 'package:kraken_ticker/constants/coin_data.dart';
import 'package:kraken_ticker/screens/detail_screen.dart';
import 'package:kraken_ticker/screens/ticker_screen.dart';

/// HomeScreen contains watchlist and news.
class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  Storage storage = Storage();
  Map watchlist = {};
  Map ticker = {};
  List redditNews = [];
  List bloombergNews = [];
  String filterNews = 'Reddit';

  @override
  void initState() {
    super.initState();

    _loadWatchlist();
  }

  void _loadWatchlist() async {
    List list = await storage.getWatchlist();
    if (list != null) {
      Map temp = {};
      for (var s in list) {
        temp.addAll({s: coinList[s]});
      }
      setState(() => watchlist = temp);
      _loadTickerInfo();
    }
  }

  void _loadTickerInfo() async {
    Map data = await Ticker().getTicker(watchlist);
    if (data.isNotEmpty) {
      setState(() => ticker = data);
    }
  }

  Widget _buildLineChart(String pair) {
    if (ticker.isEmpty) return Container();
    return FutureBuilder<dynamic>(
      future: Ticker().getOHLC(pair),
      builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: Text('Loading'));
        } else {
          if (snapshot.hasError)
            return Center(child: Text('Error'));
          else
            return OHLCChart(
              points: snapshot.data,
              highest:
                  snapshot.data.reduce((a, b) => a['avg'] > b['avg'] ? a : b),
            );
        }
      },
    );
  }

  void _refreshTicker(dynamic screen) async {
    bool refresh = await Navigator.push(
          context,
          MaterialPageRoute(builder: (_) => screen),
        ) ??
        true; // default to true, whether user clicked 'back' or swipe
    if (refresh) _loadTickerInfo();
  }

  Widget _buildTickerCard(dynamic pair) {
    var coin = coinList[pair];
    var coinTicker = ticker[pair];
    return GestureDetector(
      onTap: () => _refreshTicker(DetailScreen(pair)),
      child: TickerCard(
        name: (ticker.isNotEmpty) ? coin['name'] : '',
        altname: (ticker.isNotEmpty) ? coin['altname'] : '',
        bid: (ticker.isNotEmpty) ? coinTicker['bid'] : 0,
        ask: (ticker.isNotEmpty) ? coinTicker['ask'] : 0,
        icon: (ticker.isNotEmpty) ? coin['icon'] : '',
        iconBg: coin['iconBg'],
        size: 40.0,
        lineChart: _buildLineChart(pair),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: 64.0,
        backgroundColor: Colors.white,
        shadowColor: Colors.transparent,
        actions: [
          Padding(
            padding: EdgeInsets.symmetric(
              horizontal: 16.0,
            ),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(
                    Icons.store,
                    color: kAccentColor,
                  ),
                  color: Colors.black12,
                  splashColor: Colors.black12,
                  tooltip: 'Kraken Market',
                  iconSize: 32.0,
                  onPressed: () => _refreshTicker(TickerScreen()),
                  highlightColor: Colors.transparent,
                ),
              ],
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'DON\'T FOMO',
                style: kLabelBigBoldStyle,
              ),
              NewsFilter(),
              Text('WATCHLIST', style: kLabelBigBoldStyle),
              (watchlist.isNotEmpty)
                  ? Container()
                  : Container(
                      padding: EdgeInsets.all(20.0),
                      child: Center(
                        child: Column(
                          children: [
                            Image.asset(
                              'images/krakenseye.png',
                              width: 48.0,
                              color: Colors.black12,
                            ),
                            Padding(
                              padding: const EdgeInsets.all(16.0),
                              child: Text('WOW MUCH EMPTY WATCHLIST',
                                  textAlign: TextAlign.center,
                                  style: kLabelPlaceholderStyle),
                            ),
                            PillButton(
                              label: 'Explore Kraken Market',
                              onPressed: () => Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => TickerScreen())),
                            ),
                          ],
                        ),
                      ),
                    ),
              Column(
                children: watchlist.entries
                    .map((pair) => _buildTickerCard(pair.key))
                    .toList(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
