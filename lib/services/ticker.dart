import 'networking.dart';

final ohlcInterval = 1440;

class Ticker {
  NetworkingHelper helper = NetworkingHelper();

  /// Fetches current ticker info.
  /// Returns a list of ticker's pairs.
  Future getTicker(Map coinList) async {
    List<String> pairList = [];
    Map result = {};

    coinList.forEach((key, value) => pairList.add("$key"));

    var response = await helper.getTickerData(pairs: pairList.join(','));
    if (response['data'] != null) {
      var data = response['data'] as Map;
      for (String symbol in data.keys) {
        double a = double.parse(data[symbol]['a'][0]);
        double b = double.parse(data[symbol]['b'][0]);
        double c = double.parse(data[symbol]['c'][0]);
        double v = double.parse(data[symbol]['v'][0]);
        int t = data[symbol]['t'][0] * 1;
        double l = double.parse(data[symbol]['l'][0]);
        double h = double.parse(data[symbol]['h'][0]);
        double o = double.parse(data[symbol]['o']);
        result[symbol] = Pair(a, b, c, v, t, l, h, o).toMap();
      }
    }
    return result;
  }

  /// Fetches last 90 days OHLC data with 1440 mins interval.
  /// Returns a list of ticker's pairs.
  Future getOHLC(String pair) async {
    var now = DateTime.now();
    var monthAgoTS =
        (DateTime(now.year, now.month, now.day-90).millisecondsSinceEpoch * 0.001).toInt();

    var response = await helper.getAssetOHLCData(
      pair: pair,
      interval: ohlcInterval,
      since: monthAgoTS,
    );
    var data = response['data'];
    if (data != null) {
      var points = data[pair].map((row) {
        double timestamp = row[0].toDouble();
        double o = double.parse(row[1]);
        double h = double.parse(row[2]);
        double l = double.parse(row[3]);
        double c = double.parse(row[4]);
        double avg = (o + h + l + c) / 4;
        return OHLCAvg(timestamp, avg).toMap();
      }).toList();
      return points;
    }
  }
}

class Pair {
  double _a; // ask price
  double _b; // bid price
  double _c; // last trade closed
  double _v; // today's volume
  int _t; // today's number of trades
  double _l; // today's low
  double _h; // today's high
  double _o; // opening price

  Pair(this._a, this._b, this._c, this._v, this._t, this._l, this._h, this._o);

  Map<String, dynamic> toMap() {
    return {
      'ask': _a,
      'bid': _b,
      'last_close': _c,
      'volume': _v,
      'trades': _t,
      'low': _l,
      'high': _h,
      'open': _o,
    };
  }
}

class OHLCAvg {
  double _avg;
  double _timestamp;

  OHLCAvg(this._timestamp, this._avg);

  Map toMap() {
    return {
      'timestamp': _timestamp,
      'avg': _avg,
    };
  }
}
