import 'networking.dart';

final redditLimit = 10;

class News {
  NetworkingHelper helper = NetworkingHelper();

  /// Fetches the first 10 hot posts on r/CryptoCurrency 
  Future getRedditPost() async {
    var response = await helper.getTopRedditCrypto(redditLimit);
    List result = [];
    if (response['data'] != null) {
      for (var post in response['data']) {
        String title = post['data']['title'];
        String url = post['data']['url'];
        double createdUtc = post['data']['created_utc'];
        bool stickied = post['data']['stickied'];

        if (!stickied)
          result.add({
            'title': title,
            'url': url,
            'created_utc:': createdUtc.toInt(),
          });
      }
    }
    return result;
  }

  /// Fetches Bloomberg recent tweets
  Future getBloombergTweets() async {
    String query = 'from:crypto -is:retweet -has:mentions';
    var response = await helper.getRecentTweets(query);
    var data = response['data'];
    return data;
  }

  Future getDevTweets() async {
    String query =
        'from:VitalikButerin OR from:BrendanEich,IOHK_Charles,SatoshiLite,justinsuntron,ethereumJoseph,gavinandresen -is:retweet -has:mentions';
    var response = await helper.getRecentTweets(query);
    var data = response['data'];
    return data;
  }
}
