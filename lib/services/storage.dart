import 'package:shared_preferences/shared_preferences.dart';

class Storage {
  /// Stores watchlist to local storage
  Future<bool> storeWatchlist(String cryptoName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> watchlist = prefs.getStringList('watchlist') ?? [];
    bool exist = watchlist.contains(cryptoName);
    if (exist) {
      watchlist.remove(cryptoName);
    } else {
      watchlist.add(cryptoName);
    }
    return await prefs
        .setStringList('watchlist', watchlist)
        .then((bool success) {
      return success;
    });
  }

  // Fetches watchlist from local storage
  Future<List<String>> getWatchlist() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList('watchlist') ?? [];
  }

  // Checks whether specified coin/token is on the watchlist
  Future<bool> checkWatchlistExist(String cryptoName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> watchlist = prefs.getStringList('watchlist') ?? [];
    return watchlist.contains(cryptoName);
  }
}
