import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

const KRAKEN_TICKER_URL = 'https://api.kraken.com/0/public/Ticker';
const KRAKEN_OHLC_URL = 'https://api.kraken.com/0/public/OHLC';
const SUBREDDIT_CRYPTO_URL = 'https://www.reddit.com/r/CryptoCurrency/hot.json';
const TWITTER_SEARCH_URL = 'https://api.twitter.com/2/tweets/search/recent';

class NetworkingHelper {
  /// Formats return result to {error, data}
  Map _format({String error, dynamic result}) =>
      {'error': error, 'data': result};

  /// Fetches ticker data (asset/USD pair) from Kraken.
  Future<dynamic> getTickerData({String pairs}) async {
    try {
      Response response = await Dio().get(
        KRAKEN_TICKER_URL,
        queryParameters: {'pair': pairs},
        options: Options(
          responseType: ResponseType.json,
        ),
      );
      return _format(result: response.data['result']);
    } catch (e) {
      return _format(error: '$e');
    }
  }

  /// Fetches OHLC data from Kraken.
  Future<dynamic> getAssetOHLCData(
      {String pair, int interval, int since}) async {
    try {
      Response response = await Dio().get(
        KRAKEN_OHLC_URL,
        queryParameters: {
          'pair': pair,
          'interval': interval,
          'since': since,
        },
        options: Options(
          responseType: ResponseType.json,
        ),
      );
      return _format(result: response.data['result']);
    } catch (e) {
      return _format(error: '$e');
    }
  }

  /// Fetches latest hot posts on r/CryptoCurrency
  Future<dynamic> getTopRedditCrypto(int limit) async {
    try {
      Response response = await Dio().get(
        SUBREDDIT_CRYPTO_URL,
        queryParameters: {'limit': limit},
        options: Options(
          responseType: ResponseType.json,
        ),
      );
      return _format(result: response.data['data']['children']);
    } catch (e) {
      return _format(error: '$e');
    }
  }

  /// Fetches recent tweets
  Future<dynamic> getRecentTweets(String query) async {
    try {
      await dotenv.load(fileName: ".env");
      Response response = await Dio().get(
        TWITTER_SEARCH_URL,
        queryParameters: {
          'query': query,
          'expansions': 'author_id',
          'tweet.fields': 'author_id',
          'user.fields': 'username,created_at,public_metrics',
        },
        options: Options(
          headers: {
            'User-Agent': 'v2RecentSearchJS',
            'Authorization': "Bearer $dotenv.env['TWITTER_TOKEN'];",
          },
          responseType: ResponseType.json,
        ),
      );
      return _format(result: response.data['data']);
    } catch (e) {
      return _format(error: '$e');
    }
  }
}
