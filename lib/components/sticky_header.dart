import 'package:flutter/material.dart';
import 'package:kraken_ticker/constants/styles.dart';

class StickyHeader extends StatefulWidget {
  final top;
  final height;
  final icon;
  final title;
  final titleStyle;
  final subTitle;
  final subTitleStyle;
  final altTitle;
  final altTitleStyle;
  final rightTitleComponent;
  final rightTopComponent;
  StickyHeader({
    this.top,
    this.height,
    this.icon,
    this.title,
    this.subTitle,
    this.altTitle,
    this.titleStyle,
    this.subTitleStyle,
    this.altTitleStyle,
    this.rightTitleComponent,
    this.rightTopComponent,
  });

  @override
  _StickyHeaderState createState() => _StickyHeaderState();
}

class _StickyHeaderState extends State<StickyHeader> {
  var top;
  var height;
  var icon;

  _buildBackButton(bool showLabel) {
    return TextButton(
      child: Row(children: [
        Icon(
          Icons.arrow_back_ios_rounded,
          color: kAccentColor,
          size: 16.0,
        ),
        Text(
          'Back',
          style: showLabel ? kLabelAccentBoldStyle : kLabelTransparent,
        ),
      ]),
      onPressed: () => Navigator.pop(context, true),
      style: ButtonStyle(
        padding: MaterialStateProperty.all(EdgeInsets.all(0.0)),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    setState(() => top = widget.top);

    return SliverAppBar(
      automaticallyImplyLeading: false,
      backgroundColor: Colors.white,
      expandedHeight: widget.height,
      floating: false,
      pinned: true,
      flexibleSpace: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        top = constraints.biggest.height;
        return Container(
          child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 24.0,
            ),
            child: Column(
              children: top < widget.height
                  ? [
                      Row(
                        children: [
                          _buildBackButton(false),
                          Expanded(
                            child: Container(
                              // transform: Matrix4.translationValues(-32.0, 0.0, 0.0),
                              child: Text(
                                '${widget.altTitle}',
                                style: widget.altTitleStyle,
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ),
                          widget.rightTopComponent ?? SizedBox(width: 64.0),
                        ],
                      ),
                    ]
                  : [
                      Row(children: [
                        _buildBackButton(true),
                        Expanded(
                          child: Container(
                            child: widget.rightTopComponent ??
                                SizedBox(width: 0.0),
                            alignment: Alignment.topRight,
                          ),
                        ),
                      ]),
                      Row(
                        children: [
                          widget.icon ?? SizedBox(width: 0),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('${widget.title}', style: widget.titleStyle),
                              Text(
                                '${widget.subTitle}',
                                style: widget.subTitleStyle,
                              ),
                            ],
                          ),
                          Expanded(
                              child: widget.rightTitleComponent ?? Container()),
                        ],
                      ),
                    ],
            ),
          ),
        );
      }),
    );
  }
}
