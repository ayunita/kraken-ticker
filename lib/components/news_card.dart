import 'package:flutter/material.dart';
import 'package:kraken_ticker/constants/styles.dart';

class NewsCard extends StatelessWidget {
  final text;
  final color;
  final onTap;
  final icon;

  NewsCard({this.text, this.color, this.icon, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Container(
        margin: EdgeInsets.only(right: 16.0),
        decoration: new BoxDecoration(
          color: this.color,
          borderRadius: new BorderRadius.all(
            Radius.circular(16.0),
          ),
        ),
        width: 180.0,
        child: InkWell(
          onTap: this.onTap,
          splashColor: Colors.black12,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Stack(
              children: [
                Text(
                  '$text',
                  softWrap: false,
                  maxLines: 6,
                  overflow: TextOverflow.ellipsis,
                  style: kNewsLabelStyle,
                ),
                Positioned(
                  bottom: 0,
                  right: 0,
                  child: icon ?? Container(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
