import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kraken_ticker/constants/styles.dart';

class TickerCard extends StatelessWidget {
  final String name;
  final String altname;
  final double bid;
  final double ask;
  final String icon;
  final Color iconBg;
  final double size;
  final double paddingH;
  final Widget lineChart;

  TickerCard(
      {this.name,
      this.altname,
      this.bid,
      this.ask,
      this.lineChart,
      this.icon,
      this.iconBg,
      this.paddingH,
      this.size});

  _formatNumber(num) => NumberFormat.decimalPattern().format(num);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: paddingH ?? 0.0),
      width: double.infinity,
      height: 100,
      child: Container(
        padding: EdgeInsets.symmetric(
          vertical: 8.0,
        ),
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(color: Colors.black12),
          ),
        ),
        child: Row(
          children: [
            Container(
              width: this.size,
              height: this.size,
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: this.iconBg,
                borderRadius:
                    new BorderRadius.all(Radius.circular(this.size / 2)),
              ),
              child: Center(
                child: Text(
                  this.icon,
                  style: TextStyle(
                    fontFamily: 'Cryptofont',
                    color: Colors.white,
                    fontSize: this.size * 0.5,
                  ),
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                    ),
                    child: Text(
                      this.altname,
                      style: kLabelBoldStyle,
                    ),
                  ),
                  SizedBox(height: 4.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 16.0,
                    ),
                    child: Text(
                      this.name,
                      style: kLabelSmallStyle,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(flex: 1, child: lineChart,),
            Expanded(
              flex: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text('\$'),
                  Text(
                    '${ask.toStringAsFixed(3)}',
                    style: kLabelBigBoldStyle,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
