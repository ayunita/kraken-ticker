import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:kraken_ticker/constants/styles.dart';

class OHLCChart extends StatefulWidget {
  OHLCChart(
      {this.points,
      this.touchEnabled,
      this.showAxis,
      this.interval,
      this.highest});
  final points;
  final touchEnabled;
  final showAxis;
  final interval;
  final highest;

  @override
  _OHLCChartState createState() => _OHLCChartState();
}

class _OHLCChartState extends State<OHLCChart> {
  List points;
  double highest;

  @override
  void initState() {
    super.initState();
    setState(() {
      points = widget.points;
      highest = widget.highest['avg'];
    });
  }

  List<Color> gradientColors = [
    kPrimaryColor,
    kAccentColor,
  ];

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1,
      child: Container(
        child: LineChart(
          mainData(),
        ),
      ),
    );
  }

  LineChartData mainData() {
    return LineChartData(
      titlesData: FlTitlesData(
        show: widget.showAxis ?? false,
        bottomTitles: SideTitles(
          showTitles: true,
          interval: 2678400,
          getTextStyles: (value) => kGraphLabelStyle,
          getTitles: (value) {
            var x = value * 1000;
            DateTime date = DateTime.fromMillisecondsSinceEpoch(x.toInt());
            List months = [
              'JAN',
              'FEB',
              'MAR',
              'APR',
              'MAY',
              'JUN',
              'JUL',
              'AUG',
              'SEP',
              'OXT',
              'NOV',
              'DEC'
            ];
            return months[date.month-1];
          },
          reservedSize: 22,
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          reservedSize: 24,
          getTitles: (value) {
            return '';
          }
        ),
        rightTitles: SideTitles(
          showTitles: true,
          interval: highest / 2,
          getTextStyles: (value) => kGraphLabelStyle,
          getTitles: (value) {
            if (value >= 1000)
              return '${(value / 1000).toStringAsFixed(1)}k';
            else
              return value.toStringAsFixed(1);
          },
          reservedSize: 48,
          margin: 8,
        ),
      ),
      lineTouchData: LineTouchData(
        enabled: widget.touchEnabled ?? false,
        touchTooltipData: LineTouchTooltipData(
            tooltipBgColor: kPrimaryColor,
            getTooltipItems: (List<LineBarSpot> touchedBarSpots) {
              return touchedBarSpots.map((barSpot) {
                final flSpot = barSpot;
                if (flSpot.x == 0 || flSpot.x == 6) {
                  return null;
                }
                return LineTooltipItem(
                  '${flSpot.y.toStringAsFixed(3)}',
                  const TextStyle(color: Colors.white),
                );
              }).toList();
            }),
      ),
      gridData: FlGridData(
        show: false,
        drawVerticalLine: false,
        drawHorizontalLine: false,
      ),
      borderData: FlBorderData(show: false),
      minY: 0,
      maxY: highest * 1.2,
      lineBarsData: [
        LineChartBarData(
          spots: (points != null)
              ? points.map((p) {
                  return FlSpot(p['timestamp'], p['avg']);
                }).toList()
              : [FlSpot(0.0, 0.0)],
          isCurved: true,
          colors: gradientColors,
          barWidth: 1,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }
}
