import 'package:flutter/material.dart';
import 'package:kraken_ticker/constants/styles.dart';

class PillButton extends StatelessWidget {
  final label;
  final color;
  final padding;
  final radius;
  final bgColor;
  final textStyle;
  final onPressed;

  PillButton({this.label, this.color, this.padding, this.radius, this.bgColor, this.textStyle, this.onPressed});

  @override
  Widget build(BuildContext context) {
    return TextButton(
        onPressed: this.onPressed,
        child: Text('$label'),
        style: ButtonStyle(
    padding: MaterialStateProperty.all(
      EdgeInsets.all(this.padding ?? 16.0),
    ),
    foregroundColor: MaterialStateProperty.all(this.color ?? Colors.white),
    backgroundColor: MaterialStateProperty.all(this.bgColor ?? kAccentColor),
    textStyle: MaterialStateProperty.all(this.textStyle ?? kLabelBoldStyle),
    shape: MaterialStateProperty.all(
      RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(this.radius ?? 24.0),
        side: BorderSide(color: this.bgColor ?? kAccentColor),
      ),
    ),
        ),
      );
  }
}
