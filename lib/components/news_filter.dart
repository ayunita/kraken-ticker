import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kraken_ticker/constants/styles.dart';
import 'package:kraken_ticker/services/news.dart';
import 'package:kraken_ticker/components/news_card.dart';
import 'package:kraken_ticker/components/pill_button.dart';

class NewsFilter extends StatefulWidget {
  @override
  _NewsFilterState createState() => _NewsFilterState();
}

class _NewsFilterState extends State<NewsFilter> {
  List redditNews = [];
  List bloombergNews = [];
  String filterNews = 'Reddit';

  @override
  void initState() {
    super.initState();

    _loadRedditPost();
    _loadBloombergTweet();
  }

  void _loadRedditPost() async {
    List list = await News().getRedditPost();
    if (list != null) {
      setState(() => redditNews = list);
    }
  }

  void _loadBloombergTweet() async {
    List list = await News().getBloombergTweets();
    if (list != null) {
      setState(() => bloombergNews = list);
    }
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(Uri.encodeFull(url)) : throw 'Could not launch $url';

  Widget _buildRedditCard(dynamic post) {
    return NewsCard(
      text: '${post['title']}',
      color: Color(0x1FFF4500),
      icon: FaIcon(
        FontAwesomeIcons.redditAlien,
        color: Color(0xFFFF4500),
      ),
      onTap: () => _launchURL(post['url']),
    );
  }

  Widget _buildBloombergCard(dynamic post) {
    return NewsCard(
      text: "${post['text'].split('https://t.co/')[0]}",
      color: Colors.indigo.shade50,
      icon: FaIcon(
        FontAwesomeIcons.twitter,
        color: Color(0xFF00ACEE),
      ),
      onTap: () =>
          _launchURL("https://twitter.com/crypto/status/${post['id']}"),
    );
  }

  List<Widget> _getNewsCard() {
    switch (filterNews) {
      case 'Reddit':
        return redditNews.map((post) => _buildRedditCard(post)).toList();
      case 'Bloomberg':
        return bloombergNews.map((post) => _buildBloombergCard(post)).toList();
      default:
        return redditNews.map((post) => _buildRedditCard(post)).toList();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 8.0),
          height: 36.0,
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: [
              PillButton(
                label: 'r/CryptoCurrency',
                padding: 8.0,
                color: Colors.white,
                bgColor:
                    filterNews == 'Reddit' ? Color(0xFFFF4500) : Colors.grey,
                textStyle: kLabelSmallStyle,
                onPressed: () => setState(() => filterNews = 'Reddit'),
              ),
              SizedBox(width: 16.0),
              PillButton(
                label: '@crypto',
                padding: 8.0,
                color: Colors.white,
                bgColor:
                    filterNews == 'Bloomberg' ? Colors.indigo : Colors.grey,
                textStyle: kLabelSmallStyle,
                onPressed: () => setState(() => filterNews = 'Bloomberg'),
              ),
              SizedBox(width: 16.0),
              // PillButton(
              //   label: 'Dev',
              //   padding: 8.0,
              //   color: Colors.white,
              //   bgColor: filterNews == 'Dev'
              //       ? Colors.cyan.shade600
              //       : Colors.grey,
              //   textStyle: kLabelSmallStyle,
              //   onPressed: () {},
              // ),
            ],
          ),
        ),
        Container(
          height: 175.0,
          margin: EdgeInsets.only(top: 8.0, bottom: 24.0),
          child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: _getNewsCard(),
                ),
        ),
      ],
    );
  }
}
