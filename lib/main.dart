import 'package:flutter/material.dart';
import 'package:kraken_ticker/constants/styles.dart';
import 'package:kraken_ticker/screens/home_screen.dart';
import 'package:kraken_ticker/screens/ticker_screen.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Krakenout',
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        accentColor: kAccentColor,
        backgroundColor: kBackgroundColor,
        fontFamily: 'Quicksand',
      ),
      home: HomeScreen(),
    );
  }
}
