# Krakenseye

![picture](android/app/src/main/res/mipmap-hdpi/ic_launcher.png)

Kraken crypto market watchlist maker.

![demo](demo.gif)

## Getting Started

Prerequisites:

*  [flutter](https://flutter.dev/)
*  emulator/real device

### Installing

Get all dependencies
```
flutter pub get
```

Create ```.env``` file and specify TWITTER_TOKEN
```
TWITTER_TOKEN=
```

Run the app
```
flutter run
```

## Author

Andriani Yunita - [Bitbucket](https://bitbucket.org/ayunita)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details